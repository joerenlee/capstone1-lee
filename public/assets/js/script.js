// typing animation script
var typed = new Typed(".typing", {
	strings: ["A Full Stack Web Developer", "and I love coding!"],
	typeSpeed: 100,
	backspeed: 60,
	loop: true
});



const menu = document.querySelector(".menu");
		const menuBtn = document.querySelector(".menu-btn");
		const cancelBtn = document.querySelector(".cancel-btn");
		menuBtn.onclick = ()=>{
		menu.classList.add("active"); 
		menuBtn.classList.add("hide");
		$('body').css('overflow', 'hidden');
        $(".icon.cancel-btn").css('color', '#ffffff');
        $('.menu li a').css('color', '#ffffff')
        $('.navbar .menu').css('background', '#05386B')
		}

		cancelBtn.onclick = ()=>{
		menu.classList.remove("active");
		menuBtn.classList.remove("hide");
		$('body').css('overflow', 'auto');
		$('.menu li a').css('color', '#05386B')
		$('.navbar .menu').css('background', '#ffffff')
		}
	


$('.menu-btn').click(function () {
    $('.container').css('filter', 'blur(10px)');
    $('#about').css('filter', 'blur(10px)');
    $('#project').css('filter', 'blur(10px)');
});
$('.cancel-btn').click(function () {
    $('.container').css('filter', 'blur(0px)');
    $('#about').css('filter', 'blur(0px)');
    $('#project').css('filter', 'blur(0px)');
});